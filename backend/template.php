<?php 

$traffic_startAddress = getConfigValue('traffic_startAddress');
$traffic_destinationAddress = getConfigValue('traffic_destinationAddress');
$traffic_apiKey = getConfigValue('traffic_apiKey');

?>
<h6><?php echo _('traffic_apiKey'); ?></h6>
<input type="text" id="traffic_apiKey" placeholder="<?php echo _('traffic_apiKey');?>" value="<?php echo $traffic_apiKey; ?>"/>

<h6><?php echo _('traffic_startaddress'); ?></h6>
<input type="text" id="traffic_startAddress" placeholder="<?php echo _('traffic_startAddress_placeholder');?>" value="<?php echo $traffic_startAddress; ?>"/>

<h6><?php echo _('traffic_destinationAddress'); ?></h6>
<input type="text" id="traffic_destinationAddress" placeholder="<?php echo _('traffic_destinationAddress_placeholder');?>" value="<?php echo $traffic_destinationAddress; ?>"/>

<div id="traffic_error" style="color:red"></div>
<div id="traffic_ok" style="color:green"></div><br />

<div class="block__add" id="traffic__edit">
	<button class="traffic__edit--button" href="#">
		<span><?php echo _('save'); ?></span>
	</button>
</div>