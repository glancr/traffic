$('#traffic__edit').click(function() {
	traffic_apiKey = $("#traffic_apiKey").val();
	traffic_startAddress = $("#traffic_startAddress").val();
	traffic_destinationAddress = $("#traffic_destinationAddress").val();
	

	var url = "http://dev.virtualearth.net/REST/v1/Routes?";
	url = url + "wayPoint.1=" + traffic_startAddress + "&wayPoint.2=" + traffic_destinationAddress + "&key=" + traffic_apiKey;

	$.ajax({
		url: url,
		dataType: "jsonp",
		jsonp: "jsonp",
		success: function (r) {
			SaveCallback(r);
		},
		error: function (e) {
			$("#traffic_error").append("error!");
		}
    });
});

function SaveCallback(data)
{
	if (data.statusCode == 200) 
	{
		$.post('setConfigValueAjax.php', {'key': 'traffic_startAddress', 'value' : traffic_startAddress});
		$.post('setConfigValueAjax.php', {'key': 'traffic_destinationAddress', 'value': traffic_destinationAddress});
		$.post('setConfigValueAjax.php', {'key': 'traffic_apiKey', 'value': traffic_apiKey});
		$.post('setConfigValueAjax.php', {'key': 'traffic_ok', 'value': true});
		$.post('setConfigValueAjax.php', {'key': 'reload', 'value': 1});

		$('#ok').show(30, function() {
			$(this).hide('slow');
		});

		$("#traffic_ok").text("Daten erfolgreich gespeichert");
		$("#traffic_error").text("");

	} else {
		$('#error').show(30, function() {
			$(this).hide('slow');
		});

		$("#traffic_ok").text("");
		$("#traffic_error").text(data.statusDescription);
		$.post('setConfigValueAjax.php', {'key': 'traffic_ok', 'value': data.statusDescription});
	}
}