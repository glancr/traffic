var incidentObj = {};
var incidentArray = [];
var i=0;

$(document).ready(function () {
	reloadTrafficIncidents();
});

function reloadTrafficIncidents() {

	var url = "http://dev.virtualearth.net/REST/v1/Routes?";
	var ok = "<?php echo getConfigValue('traffic_ok'); ?>";
	var key = "<?php echo getConfigValue('traffic_apiKey'); ?>";
	var startAddress = "<?php echo getConfigValue('traffic_startAddress'); ?>";
	var destinationAddress = "<?php echo getConfigValue('traffic_destinationAddress'); ?>";

	if(ok == "true")
	{
		url = url + "wayPoint.1=" + startAddress + "&wayPoint.2=" + destinationAddress + "&key=" + key;

		$.ajax({
			url: url,
			dataType: "jsonp",
			jsonp: "jsonp",
			success: function (r) {
				TrafficCallback(r, startAddress, destinationAddress);
			},
			error: function (e) {
				$("#traffic_error").append("error!");
			}
		});				
		// update every 15 min
		window.setTimeout(function() {
			reloadTrafficIncidents();
		}, 900000);	

		window.setInterval(function()
		{
			if(incidentArray.length > 4)
			{
				if(i < incidentArray.length)
				{			
					updateTrafficTable(i);
					i=i+1;
				}
				else
				{
					i=0;
				}
			}
			else
			{
				i=0;
			}
		}, 4000);		
	} 
	else {
		$("#traffic_table").append("<?php echo _('traffic_error'); ?>" + ok);
	}
}

function updateTrafficTable(i)
{
	$('#traffic_table > tbody > tr').first().hide('slow', function() 
	{ 
		$('#traffic_table > tbody > tr').first().remove(); 	
		$('<tr><td>' + translate(incidentArray[i]._streetName) + '</td><td>' + translate(incidentArray[i]._originAddress) + '</td><td>' + translate(incidentArray[i]._warningText) + '</td></tr>').hide().appendTo('#traffic_table > tbody').show('slow');
	});
}

function TrafficCallback(data, startAddress, destinationAddress)
{
	if (data.statusCode == 200) 
	{
		if (data.statusDescription == "OK")
		{
			var route = data.resourceSets[0].resources[0];
			var routeLegs = route.routeLegs[0];
			var itineraryItemsArr = routeLegs.itineraryItems;
			var distanceUnit = route.distanceUnit;
			var travelDistance = Math.floor(route.travelDistance);
			var travelDuration = route.travelDuration;
			var travelDurationTraffic = route.travelDurationTraffic;
			var travelDurationTrafficMin = Math.floor(parseInt(travelDurationTraffic, 10) / 60);
			var startLocation = routeLegs.startLocation.name;
			var endLocation = routeLegs.endLocation.name;
							
			if(typeof itineraryItemsArr !== 'undefined' && itineraryItemsArr !== 'null')
			{
				$("#traffic_table").empty();
				incidentArray = [];
				$("#traffic_subtitle").text(startAddress + " -> " + destinationAddress + " - " + travelDistance + " " + distanceUnit + " - " + travelDurationTrafficMin + " min");
				
				$.each(itineraryItemsArr, function(index, el)
				{
					var warnings = el.warnings;
					if(warnings != null)
					{
						$.each(warnings, function(index, element) {
							var warningType = element.warningType;
							var warningText = removeWords(element.text);
							
							if(warningType == 'Accident' 
							|| warningType == 'BlockedRoad' 
							|| warningType == 'RoadClosures' 
							|| warningType == 'RoadHazard'
							|| warningType == 'ScheduledConstruction'
							|| warningType == 'SeasonalClosures'
							|| warningType == 'Weather'
							|| warningType == 'OtherNews'
							|| warningType == 'Other'
							|| warningType == 'DisabledVehicle'
							|| warningType == 'Congestion'
							|| warningType == 'TrafficFlow')
							{
								var names = "";
								var rmvEuNameRegex = /[^E0-9]/;
								$.each(el.details[el.details.length-1].names, function(index, name) 
								{
									name = removeWords(name);
									if(name != "")
									{	
										if(names.trim())
										{
											names = names + ' / ';
										}
										names = names + name;
									}
								});
								
								var origin = element.origin;
								getAddress(origin, names, warningType, warningText);
							}
						});
					}
				});
			}
		}
		else {
			var errorDesc = data.statusDescription;
			if(data.errorDetails.length > 0)
			{     					
				errorDesc = data.errorDetails[0];
			}
			$.post('/config/setConfigValueAjax.php', {'key' : 'traffic_ok', 'value' : errorDesc});
			location.reload();
		}				
	}
}

function getAddress(gpsCoordinates, streetName, warningType, warningText)
{
	var key = "<?php echo getConfigValue('traffic_apiKey'); ?>";
	var url = "http://dev.virtualearth.net/REST/v1/Locations/"+gpsCoordinates+"?o=json&key="+key;
	var originAddress =  "";
	var locality = "";
	jQuery.ajax(
	{
        url: url,
		dataType: "jsonp",
		jsonp: "jsonp",
        success: function (data) 
		{
            if (data.statusCode == 200) 
			{
				if (data.statusDescription == "OK")
				{
						
					var addressLine = data.resourceSets[0].resources[0].address.addressLine;
					locality = data.resourceSets[0].resources[0].address.locality;
					var splittedAddress = addressLine.split('&');		
					
					$.each(splittedAddress, function(index, name) {
						name = name.trim();
						if((index == 0 && !streetName.search("/" + name + "/")) || index > 0 )
						{
							name = removeWords(name);
							if(name != "")
								{if(originAddress.trim())
								{	
									originAddress = originAddress + ' / ';
								}
								originAddress = originAddress + name;
							}
						}
					});
					
					if(originAddress.trim() != "")
					{
						originAddress = "<ul id=\"traffic_table_originList\"><li id=\"traffic_table_place\">"+ originAddress + "</li><li id=\"traffic_table_locality\"> " + locality + "</li></ul>";
					}
					else
					{
						originAddress = locality;
					}
					
					incidentObj = {
						_streetName: streetName,
						_originAddress: originAddress,
						_warningType: warningType,
						_warningText: warningText
					}
					incidentArray.push(incidentObj);
					
					if(incidentArray.length < 5)
					{
						$("#traffic_table").append("<tr></tr>");
						$("#traffic_table tr:last").append("<td>" + translate(incidentObj._streetName) + "</td>");
						$("#traffic_table tr:last").append("<td>" + translate(incidentObj._originAddress) + "</td>");
						$("#traffic_table tr:last").append("<td>" + translate(incidentObj._warningText) + "</td>");
					}
				}
			}			
			else {
				var errorDesc = data.statusDescription;
    			if(data.errorDetails.length > 0)
    			{     					
    				errorDesc = data.errorDetails[0];
    			}
    			$.post('/config/setConfigValueAjax.php', {'key' : 'traffic_ok', 'value' : errorDesc});
    			location.reload();
    		}
        },
		error: function (e) {
            $("#traffic_table").append("<?php echo _('traffic_error'); ?>");
        }
    });
	

}

function translate(string)
{
	string = string.replace("TrafficFlow", "<?php echo _('TrafficFlow'); ?>");
	string = string.replace("ScheduledConstruction", "<?php echo _('ScheduledConstruction'); ?>");
	string = string.replace("BlockedRoad", "<?php echo _('BlockedRoad'); ?>");	
	string = string.replace("Minor Accident", "<?php echo _('MinorAccident'); ?>");
	string = string.replace("Serious Accident", "<?php echo _('SeriousAccident'); ?>");
	string = string.replace("Accident", "<?php echo _('Accident'); ?>");
	string = string.replace("RoadClosures", "<?php echo _('RoadClosures'); ?>");
	string = string.replace("RoadHazard", "<?php echo _('RoadHazard'); ?>");
	string = string.replace("Road hazard", "<?php echo _('RoadHazard'); ?>");
	string = string.replace("SeasonalClosures", "<?php echo _('SeasonalClosures'); ?>");
	string = string.replace("Weather", "<?php echo _('Weather'); ?>");
	string = string.replace("OtherNews", "<?php echo _('OtherNews'); ?>");
	string = string.replace("Other", "<?php echo _('Other'); ?>");
	string = string.replace("DisabledVehicle", "<?php echo _('DisabledVehicle'); ?>");
	string = string.replace("Disabled vehicle", "<?php echo _('DisabledVehicle'); ?>");
	string = string.replace("Congestion", "<?php echo _('Congestion'); ?>");
	string = string.replace("Minor", "<?php echo _('Minor'); ?>");
	string = string.replace("Ramp", "<?php echo _('Ramp'); ?>");
	string = string.replace("Serious", "<?php echo _('Serious'); ?>");
	return string;
}

function removeWords(string)
{
	var rmvEuNameRegex = /(Street|Construction:|E[0-9]+( \W)?)/g;
	return string.replace(rmvEuNameRegex, "");	
}

